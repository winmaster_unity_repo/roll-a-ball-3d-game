﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public float speed;
    public int maxAmountOfPoints;
    public Text counterText;
    public Text winnerText;
    public Text loserText;
    public Text restartText;

    private Rigidbody rigidbody;
    private int counter;

    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        counter = 0;
        SetCounterValue();
        winnerText.text = ""; 
        loserText.text = "";
        restartText.text = "";
    }

    void SetCounterValue()
    {
        counterText.text = "Points: " + counter.ToString();

        if(counter >= maxAmountOfPoints)
        {
            winnerText.text = "You win!";
            restartText.text = "Press Escape button to restart game";
        }
    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rigidbody.AddForce(movement * speed);
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("MiniGame");
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            counter++;
            SetCounterValue();
        }

        if(other.gameObject.CompareTag("crashed"))
        {
            loserText.text = "You loose!";
            SceneManager.LoadScene("MiniGame");
        }
    }
}
